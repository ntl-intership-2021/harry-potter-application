import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';

import { filterReducer } from './slices/filterSlice';
import { personReducer } from './slices/personSlice';

export const store = configureStore({
  reducer: {
    personsList: personReducer,
    filters: filterReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
