import { createSlice } from '@reduxjs/toolkit';

import { IPerson } from '../../interfaces/common/IPersons';

interface IPersonsState {
  persons: IPerson[] | [];
}

const personsState: IPersonsState = {
  persons: [],
};

export const personSlice = createSlice({
  name: 'persons',
  initialState: personsState,
  reducers: {
    addPersonsList(state, action): void {
      state.persons = [...action.payload];
    },
    addPerson(state, action): void {
      state.persons = [...state.persons, action.payload];
    },
    addFilteredPersonsList(state, action): void {
      state.persons = [...action.payload];
    },
  },
});

export const { addPersonsList, addPerson, addFilteredPersonsList } = personSlice.actions;
export const personReducer = personSlice.reducer;
