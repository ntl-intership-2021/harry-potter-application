import { createSlice } from '@reduxjs/toolkit';

import { IFilterSettings } from '../../interfaces/common/IFilterSettings';

interface IFiltersState {
  filtersDictionary: IFilterSettings;
}

const filtersState: IFiltersState = {
  filtersDictionary: {
    gender: [],
    race: [],
    side: [],
  },
};

export const filterSlice = createSlice({
  name: 'filters',
  initialState: filtersState,
  reducers: {
    addFilterSettings(state, action) {
      state.filtersDictionary = { ...action.payload };
    },
  },
});

export const { addFilterSettings } = filterSlice.actions;
export const filterReducer = filterSlice.reducer;
