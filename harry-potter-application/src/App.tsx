import React from 'react';

import classes from './App.module.scss';
import { Header } from './components';
import { Router } from './router/Router';

const App: React.FC = () => {
  return (
    <div className={classes.app}>
      <Header />
      <Router />
    </div>
  );
};

export { App };
