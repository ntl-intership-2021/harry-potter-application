import React from 'react';

import { useHistory } from 'react-router-dom';

import { Button } from '../../components/Button/Button';

import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';

import classes from './NotFound.module.scss';

const NotFound: React.FC = (): JSX.Element => {
  const history = useHistory();
  return (
    <section className={classes.notFound}>
      <div className={classes.content}>
        <h1 className={classes.header}>404</h1>
        <h2>Ошибка 404. Такая страница не существует либо она была удалена</h2>
        <Button
          type={ButtonTypes.BUTTON}
          styles={ButtonStylesTypes.beige}
          text={'На главную'}
          handler={(): void => history.push('/main')}
        />
      </div>
    </section>
  );
};

export { NotFound };
