import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import { Form } from '../../components/Form/Form';
import { Slider } from '../../components/Slider/Slider';
import { IPerson } from '../../interfaces/common/IPersons';
import { PersonsProvider } from '../../services/persons/PersonsProvider';
import { addPersonsList } from '../../store/slices/personSlice';

import classes from './Persons.module.scss';

const Persons: React.FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const [personsIsLoaded, setPersonsIsLoaded] = useState<boolean>(false);

  useEffect(() => {
    const getPersons = async (): Promise<void> => {
      try {
        const incomingPersons: IPerson[] = await PersonsProvider.getFullListPersons();
        dispatch(addPersonsList([...incomingPersons]));
        setPersonsIsLoaded(true);
      } catch (e) {
        if (e instanceof Error) {
          throw Error(e.message);
        }
      }
    };
    void getPersons();
  }, []);

  return (
    <section className={classes.persons}>
      <div className={classes.wrapper}>
        <Form />
        {personsIsLoaded ? <Slider /> : <h1>...Is loading</h1>}
      </div>
    </section>
  );
};

export { Persons };
