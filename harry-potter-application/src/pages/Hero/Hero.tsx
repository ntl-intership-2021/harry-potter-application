import React from 'react';
import { useHistory } from 'react-router-dom';

import { Button } from '../../components/Button/Button';

import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';

import classes from './Hero.module.scss';

const Hero: React.FC = (): JSX.Element => {
  const history = useHistory();

  return (
    <section className={classes.hero}>
      <div className={classes.wrapper}>
        <div className={classes.content}>
          <h1>Найди любимого персонажа «Гарри&nbsp;Поттера»</h1>
          <h2>Вы сможете узнать тип героев, их способности, сильные стороны и недостатки.</h2>
          <Button
            type={ButtonTypes.BUTTON}
            styles={ButtonStylesTypes.beige}
            text={'Начать'}
            handler={(): void => history.push('/persons')}
          />
        </div>
      </div>
    </section>
  );
};

export { Hero };
