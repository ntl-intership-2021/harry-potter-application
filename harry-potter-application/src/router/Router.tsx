import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Hero } from '../pages/Hero/Hero';
import { NotFound } from '../pages/NotFound/NotFound';
import { Persons } from '../pages/Persons/Persons';

const Router: React.FC = () => {
  return (
    <Switch>
      <Route path="/main" exact component={Hero} />
      <Route path="/" exact component={Hero} />
      <Route path="/persons/new" exact component={Persons} />
      <Route path="/persons/:id" exact component={Persons} />
      <Route path="/persons" exact component={Persons} />
      <Route component={NotFound} />
    </Switch>
  );
};

export { Router };
