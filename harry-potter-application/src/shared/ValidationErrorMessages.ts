export enum ValidationErrorMessages {
  SHORT = 'Too Short!',
  LONG = 'Too Long!',
  REQUIRED = 'Required',
  KEYS = 'You need to enter three tag!',
  EMPTY = "This field can't be empty!",
  WRONG = 'Enter correct values',
}
