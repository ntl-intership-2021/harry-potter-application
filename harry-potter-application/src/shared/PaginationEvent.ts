export enum PaginationEvent {
  NEXT = 'next',
  PREV = 'prev',
}
