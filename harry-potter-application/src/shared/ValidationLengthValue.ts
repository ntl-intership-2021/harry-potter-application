export enum ValidationLengthValue {
  MAX = 20,
  MIN = 3,
  DESCRIPTION = 100,
  EMPTY = 0,
}
