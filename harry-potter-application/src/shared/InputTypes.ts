export enum InputTypes {
  TEXT = 'text',
  COLOR = 'color',
  FILE = 'file',
  SELECT = 'select',
}
