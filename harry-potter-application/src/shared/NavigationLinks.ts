import { INavigationLink } from '../interfaces/application/INavigationLink';

export const NAVIGATION_LINKS: INavigationLink[] = [
  {
    rus: 'главная',
    eng: 'main',
  },
  {
    rus: 'персонажи',
    eng: 'persons',
  },
];
