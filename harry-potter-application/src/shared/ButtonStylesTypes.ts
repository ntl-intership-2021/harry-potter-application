import { IButtonStylesTypes } from '../interfaces/application/IButtonStylesTypes';

export const ButtonStylesTypes: IButtonStylesTypes = {
  beige: 'button__beige',
  green: 'button__green',
  icon: 'button__icon',
  white: 'button__icon-white',
  active: 'active',
};
