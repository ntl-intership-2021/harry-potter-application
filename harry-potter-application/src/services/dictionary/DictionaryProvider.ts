import { AxiosResponse } from 'axios';

import { IFilterSettings } from '../../interfaces/common/IFilterSettings';
import { IFilterType } from '../../interfaces/common/IFilterType';
import { ApiService } from '../ApiService';

export class DictionaryProvider {
  public static async getAllDictionary(): Promise<IFilterSettings> {
    const responseGender: AxiosResponse<IFilterType[]> = await ApiService.get(`/gender`);
    const responseRace: AxiosResponse<IFilterType[]> = await ApiService.get(`/race`);
    const responseSide: AxiosResponse<IFilterType[]> = await ApiService.get(`/side`);
    return {
      gender: responseGender.data,
      race: responseRace.data,
      side: responseSide.data,
    };
  }
}
