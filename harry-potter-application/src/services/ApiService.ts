import axios from 'axios';

const ApiService = axios.create({
  baseURL: 'http://localhost:5000/api/HARRY_POTTER',
  timeout: 5000,
});

export { ApiService };
