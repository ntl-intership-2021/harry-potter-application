import { AxiosResponse } from 'axios';

import { PersonsFactory } from '../../core/PersonsFactory';
import { IPerson } from '../../interfaces/common/IPersons';
import { IDataGET200 } from '../../interfaces/server/IDataGET200';
import { ApiService } from '../ApiService';

class PersonsProvider {
  public static async getFullListPersons(): Promise<IPerson[]> {
    const response: AxiosResponse<IDataGET200> = await ApiService.get(`/character`);
    return PersonsFactory.convertPersonsArray(response.data.content);
  }

  public static async getPerson(value: string): Promise<IPerson[]> {
    const response: AxiosResponse<IDataGET200> = await ApiService.get(`/character?values=${value}`);
    return PersonsFactory.convertPersonsArray(response.data.content);
  }

  public static async getFilteredPersons(value): Promise<IPerson[]> {
    const response: AxiosResponse<IDataGET200> = await ApiService.get(`/character?${value}`);
    return PersonsFactory.convertPersonsArray(response.data.content);
  }

  public static async addPerson(person: string): Promise<void> {
    const final = person.replace(/\\/g, '');
    await ApiService.post(`/character`, final, {
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).catch((e) => {
      throw Error(e);
    });
  }
}

export { PersonsProvider };
