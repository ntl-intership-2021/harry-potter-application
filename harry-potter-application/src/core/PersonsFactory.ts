import { IPerson } from '../interfaces/common/IPersons';
import { IPersonGET200 } from '../interfaces/server/IPersonGET200';

class PersonsFactory {
  public static convertPersonsArray(persons: IPersonGET200[]): IPerson[] {
    return persons.map((person) => {
      return {
        id: person.id,
        name: person.name,
        gender: person.gender,
        race: person.race,
        side: person.side,
        backgroundColor: person.backgroundColor,
        nameColor: person.nameColor,
        parametersColor: person.parametersColor,
        img: person.imageURL,
      };
    });
  }
}

export { PersonsFactory };
