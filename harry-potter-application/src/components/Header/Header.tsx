import React from 'react';

import { Logo, Navigation } from '../index';

import classes from './Header.module.scss';

const Header: React.FC = (): JSX.Element => {
  return (
    <header className={classes.header}>
      <div className={classes.wrapper}>
        <Logo />
        <Navigation />
      </div>
    </header>
  );
};

export { Header };
