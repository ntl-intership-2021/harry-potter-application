import React, { useEffect, useState } from 'react';

import { useSelector } from 'react-redux';

import { IPerson } from '../../interfaces/common/IPersons';
import { IAppReduxState } from '../../interfaces/store/IAppReduxState';
import { ELEMENTS_VALUE } from '../../shared/ElementsValue';

import { PaginationEvent } from '../../shared/PaginationEvent';

import { CardsHolder, SliderPagination } from '../index';

import classes from './Slider.module.scss';

const Slider: React.FC = (): JSX.Element => {
  const personsList = useSelector((state: IAppReduxState) => state.personsList.persons);
  const [personsState, setPersonsState] = useState<[] | IPerson[]>([...personsList]);
  const [startIndex, setStartIndex] = useState<number>(0);
  const [endIndex, setEndIndex] = useState<number>(ELEMENTS_VALUE);
  const [activeIndex, setActiveIndex] = useState<number>(0);
  const indicatorLength = Math.ceil(personsList.length / ELEMENTS_VALUE);

  useEffect(() => {
    setPersonsState([...personsList]);
  }, [personsList]);

  const sliderCardsHandler = (event: React.MouseEvent): void => {
    const target = event.target as HTMLButtonElement;
    const eventId: string = target.id;
    const eventIdToNumber = parseInt(eventId, 10);

    if (eventId === PaginationEvent.NEXT && endIndex < personsList.length) {
      setStartIndex(startIndex + ELEMENTS_VALUE);
      setEndIndex(endIndex + ELEMENTS_VALUE);
      setActiveIndex(endIndex / ELEMENTS_VALUE);
    }

    if (eventId === PaginationEvent.PREV && startIndex > 0) {
      setStartIndex(startIndex - ELEMENTS_VALUE);
      setEndIndex(endIndex - ELEMENTS_VALUE);
      setActiveIndex(endIndex / ELEMENTS_VALUE);
    }

    if (Number.isInteger(parseInt(eventId, 10))) {
      setStartIndex(eventIdToNumber * ELEMENTS_VALUE);
      setEndIndex(eventIdToNumber * ELEMENTS_VALUE + ELEMENTS_VALUE);
      setActiveIndex(eventIdToNumber);
    }
  };

  return (
    <div className={classes.slider}>
      {personsState.length > 0 ? (
        <div style={{ display: 'contents' }}>
          <CardsHolder key={startIndex} persons={personsList.slice(startIndex, endIndex)} />
          <SliderPagination
            indicatorLength={indicatorLength}
            activeIndex={activeIndex}
            sliderCardsHandler={sliderCardsHandler}
          />
        </div>
      ) : null}
    </div>
  );
};

export { Slider };
