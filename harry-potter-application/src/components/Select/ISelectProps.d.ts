import React from 'react';

import { IFilterType } from '../../interfaces/common/IFilterType';

export interface ISelectProps {
  type: string;
  engType: string;
  options: IFilterType[];
  checkboxesHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
