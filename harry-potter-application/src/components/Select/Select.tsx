import React, { useState } from 'react';

import { ISelectProps } from './ISelectProps';
import classes from './Select.module.scss';

const Select: React.FC<ISelectProps> = ({ type, engType, options, checkboxesHandler }): JSX.Element => {
  const [expand, setExpand] = useState<boolean>(false);
  const showCheckboxes = (): void => {
    if (!expand) {
      setExpand(true);
    } else {
      setExpand(false);
    }
  };
  return (
    <div className={classes.multiselect}>
      <div className={classes.selectBox} onClick={showCheckboxes}>
        <select>
          <option value={type}>{type}</option>
        </select>
        <div className={classes.overSelect} />
      </div>
      {expand ? (
        <div className={classes.checkboxes}>
          {options.map((item, index) => {
            return (
              <label key={index} htmlFor={`${engType}__${item.id}`}>
                <input type="checkbox" value={item.value} id={`${engType}__${item.id}`} onChange={checkboxesHandler} />
                <span>{item.value}</span>
              </label>
            );
          })}
        </div>
      ) : null}
    </div>
  );
};

export { Select };
