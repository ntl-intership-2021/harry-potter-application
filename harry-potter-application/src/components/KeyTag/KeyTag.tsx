import React from 'react';

import { IKeyTagProps } from './IKeyTagProps';
import classes from './KeyTag.module.scss';

const KeyTag: React.FC<IKeyTagProps> = ({ keyTag }): JSX.Element => {
  const randNumb = (min: number, max: number): number => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  const getRandomMutedColor = (): string => {
    const min = 30;
    const max = 100;
    const range = 16;
    const mutedColor =
      '#' +
      randNumb(min, max).toString(range) +
      randNumb(min, max).toString(range) +
      randNumb(min, max).toString(range);
    return mutedColor;
  };

  return (
    <div className={classes.wrapper} style={{ backgroundColor: getRandomMutedColor() }}>
      <span className={classes.text}>{keyTag}</span>
    </div>
  );
};

export { KeyTag };
