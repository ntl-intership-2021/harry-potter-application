import { INavigationLink } from '../../interfaces/application/INavigationLink';

export interface INavigationLinkProps extends INavigationLink {}
