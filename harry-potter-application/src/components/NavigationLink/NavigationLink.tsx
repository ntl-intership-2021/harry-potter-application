import React from 'react';

import { NavLink } from 'react-router-dom';

import { INavigationLinkProps } from './INavigationLinkProps';
import classes from './NavigationLink.module.scss';

const NavigationLink: React.FC<INavigationLinkProps> = ({ rus, eng }): JSX.Element => {
  return (
    <li className={classes.item}>
      <NavLink to={`/${eng}`} className={classes.link}>
        {rus}
      </NavLink>
    </li>
  );
};

export { NavigationLink };
