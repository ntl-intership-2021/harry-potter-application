import React from 'react';

import { IPerson } from '../../interfaces/common/IPersons';

import { Card } from '../index';

import classes from './CardsHolder.module.scss';
import { ICardsHolderProps } from './ICardHolderProps';

const CardsHolder: React.FC<ICardsHolderProps> = ({ persons }) => {
  return (
    <div className={classes.cards_holder}>
      {persons.map((person: IPerson) => {
        return <Card key={person.name} person={person} clickable={true} />;
      })}
    </div>
  );
};

export { CardsHolder };
