import { IPerson } from '../../interfaces/common/IPersons';

export interface ICardsHolderProps {
  persons: IPerson[];
}
