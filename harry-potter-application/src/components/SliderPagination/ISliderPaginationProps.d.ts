import React from 'react';

export interface ISliderPaginationProps {
  indicatorLength: number;
  activeIndex: number;
  sliderCardsHandler: (event: React.MouseEvent) => void;
}
