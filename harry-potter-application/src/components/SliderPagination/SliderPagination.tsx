import React from 'react';

import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';
import { PaginationEvent } from '../../shared/PaginationEvent';
import { Button, SliderIndicator } from '../index';

import { ISliderPaginationProps } from './ISliderPaginationProps';
import classes from './SliderPagination.module.scss';

const SliderPagination: React.FC<ISliderPaginationProps> = ({ indicatorLength, activeIndex, sliderCardsHandler }) => {
  return (
    <div className={classes.pagination}>
      <Button
        styles={ButtonStylesTypes.icon}
        text="&lang;"
        type={ButtonTypes.BUTTON}
        id={PaginationEvent.PREV}
        handler={sliderCardsHandler}
      />
      {Array.from(Array(indicatorLength).keys()).map((element, index) => {
        const active = activeIndex === index;
        return (
          <SliderIndicator key={index} id={`${element}`} active={active} sliderCardsHandler={sliderCardsHandler} />
        );
      })}
      <Button
        styles={ButtonStylesTypes.icon}
        text="&rang;"
        type={ButtonTypes.BUTTON}
        id={PaginationEvent.NEXT}
        handler={sliderCardsHandler}
      />
    </div>
  );
};

export { SliderPagination };
