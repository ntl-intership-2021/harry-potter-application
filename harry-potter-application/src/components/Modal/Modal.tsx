import React from 'react';

import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';
import { Button } from '../index';

import { IModalProps } from './IModalProps';

import classes from './Modal.module.scss';

const Modal: React.FC<IModalProps> = ({ modalCloseHandler, innerElement }): JSX.Element => {
  return (
    <div className={classes.overlay}>
      <div className={classes.modal}>
        <Button styles={ButtonStylesTypes.white} text="&times;" type={ButtonTypes.BUTTON} handler={modalCloseHandler} />
        {innerElement}
      </div>
    </div>
  );
};

export { Modal };
