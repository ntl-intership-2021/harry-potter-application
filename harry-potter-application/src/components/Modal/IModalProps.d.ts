export interface IModalProps {
  modalCloseHandler: () => void;
  innerElement: JSX.Element;
}
