import React, { CSSProperties } from 'react';

import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';
import { InputTypes } from '../../shared/InputTypes';
import { Button } from '../index';

import { IInputProps } from './IInputProps';

import classes from './Input.module.scss';

const UPLOAD_STYLES_INPUT: Partial<CSSProperties> = {
  height: '200px',
  width: '200px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-end',
  alignItems: 'center',
};

const Input: React.FC<IInputProps> = ({
  onChange,
  title,
  type,
  htmlForID,
  buttonContains,
  labelContains,
}): JSX.Element => {
  return (
    <label className={classes.label} htmlFor={htmlForID}>
      {labelContains ? title : null}
      <span className={classes.search} style={type === InputTypes.FILE ? UPLOAD_STYLES_INPUT : undefined}>
        <input
          className={classes.input}
          type={type}
          placeholder={!labelContains ? title : ''}
          onChange={onChange}
          id={htmlForID}
          autoComplete={'off'}
        />
        {buttonContains ? <Button type={ButtonTypes.SUBMIT} styles={ButtonStylesTypes.beige} text="🔍" /> : null}
      </span>
    </label>
  );
};

export { Input };
