import React from 'react';

interface IInputProps {
  title: string;
  type: string;
  htmlForID: string;
  buttonContains: boolean;
  labelContains: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
