// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'active': string;
  'button': string;
  'button__beige': string;
  'button__green': string;
  'button__icon': string;
  'button__icon-white': string;
}
export const cssExports: CssExports;
export default cssExports;
