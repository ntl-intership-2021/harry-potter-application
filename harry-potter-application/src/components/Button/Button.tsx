import React from 'react';

import classes from './Button.module.scss';
import { IButtonProps } from './IButtonProps';

const Button: React.FC<IButtonProps> = ({ styles, text, handler, id, type, style }): JSX.Element => {
  const styleClasses: string = [classes.button, classes[styles]].join(' ');
  return (
    <button
      style={style ? style : undefined}
      className={styleClasses}
      type={type}
      onClick={handler}
      id={id ? id : undefined}
    >
      {text}
    </button>
  );
};

export { Button };
