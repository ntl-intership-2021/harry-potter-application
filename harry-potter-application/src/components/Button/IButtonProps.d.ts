import React, { CSSProperties } from 'react';

import { ButtonTypes } from '../../shared/ButtonTypes';

export interface IButtonProps {
  styles: string;
  text: string;
  handler?: (event: React.MouseEvent) => void;
  id?: string;
  type: ButtonTypes.BUTTON | ButtonTypes.SUBMIT;
  style?: CSSProperties | undefined;
}
