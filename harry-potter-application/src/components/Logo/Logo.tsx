import React from 'react';

import classes from './Logo.module.scss';

const Logo: React.FC = (): JSX.Element => {
  return <div className={classes.logo} />;
};

export { Logo };
