export interface IModalFormProps {
  onCloseHandler: () => void;
}
