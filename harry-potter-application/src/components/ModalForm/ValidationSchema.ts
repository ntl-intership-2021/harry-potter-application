import * as Yup from 'yup';

import { ValidationErrorMessages } from '../../shared/ValidationErrorMessages';
import { ValidationLengthValue } from '../../shared/ValidationLengthValue';

export const ValidationSchema = Yup.object().shape({
  name: Yup.string().required(ValidationErrorMessages.REQUIRED),
  gender: Yup.string().required(ValidationErrorMessages.REQUIRED),
  race: Yup.string().required(ValidationErrorMessages.REQUIRED),
  side: Yup.string().required(ValidationErrorMessages.REQUIRED),
  description: Yup.string()
    .min(ValidationLengthValue.MIN, ValidationErrorMessages.SHORT)
    .max(ValidationLengthValue.DESCRIPTION, ValidationErrorMessages.LONG)
    .required(ValidationErrorMessages.REQUIRED),
  keys: Yup.string().required(ValidationErrorMessages.REQUIRED),
  img: Yup.string()
    .min(ValidationLengthValue.EMPTY, ValidationErrorMessages.EMPTY)
    .required(ValidationErrorMessages.REQUIRED),
  color: Yup.string()
    .min(ValidationLengthValue.EMPTY, ValidationErrorMessages.EMPTY)
    .matches(/^#[\da-f]{6}$/i, ValidationErrorMessages.WRONG),
});
