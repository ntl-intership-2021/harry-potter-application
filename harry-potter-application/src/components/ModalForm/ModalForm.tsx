import { Field, Form, Formik } from 'formik';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { IFilterType } from '../../interfaces/common/IFilterType';
import { IPerson } from '../../interfaces/common/IPersons';
import { SelectHeaderTypeRUS } from '../../interfaces/common/SelectHeaderTypeRUS';
import { IPersonPOST200 } from '../../interfaces/server/IPersonPOST200';
import { IAppReduxState } from '../../interfaces/store/IAppReduxState';
import { PersonsProvider } from '../../services/persons/PersonsProvider';
import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';
import { InputTypes } from '../../shared/InputTypes';
import { addPerson } from '../../store/slices/personSlice';

import buttonStyles from '../Button/Button.module.scss';
import inputStyles from '../Input/Input.module.scss';
import { Button, Card, ModalCardContent } from '../index';

import { IModalFormProps } from './IModalFormProps';
import classes from './ModalForm.module.scss';
import { ValidationSchema } from './ValidationSchema';

enum Labels {
  NAME = 'Добавить имя',
  DESCRIPTION = 'Добавить описание',
  KEYS = 'Добавить теги',
  IMAGE = 'Добавить фото',
  BACKGROUND_COLOR = 'Цвет фона',
  NAME_COLOR = 'Цвет имени',
  PARAMETERS_COLOR = 'Цвет параметров',
}
enum HtmlForId {
  NAME = 'modal-form-name',
  DESCRIPTION = 'modal-form-description',
  KEYS = 'modal-form-tag',
  IMAGE = 'form-modal-add-imageUrl',
  BACKGROUND_COLOR = 'form-modal-add-backgroundColor',
  NAME_COLOR = 'form-modal-add-nameColor',
  PARAMETERS_COLOR = 'form-modal-add-parametersColor',
}
enum FieldName {
  NAME = 'name',
  DESCRIPTION = 'description',
  GENDER = 'gender',
  RACE = 'race',
  SIDE = 'side',
  KEYS = 'keys',
  IMAGE = 'img',
  BACKGROUND_COLOR = 'backgroundColor',
  NAME_COLOR = 'nameColor',
  PARAMETERS_COLOR = 'parametersColor',
}

const enum PreviewMode {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
}

const ModalForm: React.FC<IModalFormProps> = ({ onCloseHandler }): JSX.Element => {
  const dispatch = useDispatch();
  const filtersDictionary = useSelector((state: IAppReduxState) => state.filters.filtersDictionary);
  const [previewMode, setPreviewMode] = useState<PreviewMode | string>(PreviewMode.PRIMARY);

  const formValues: IPerson = {
    id: '',
    name: '',
    gender: '',
    race: '',
    side: '',
    description: '',
    img: '',
    backgroundColor: '#b09a81',
    nameColor: '#FFFFFF',
    parametersColor: '#FFFFFF',
    keys: '',
  };

  const onChangePreviewMode = (event: React.MouseEvent): void => {
    const target = event.target as HTMLSpanElement;
    const id: string = target.id;
    if (id !== previewMode) {
      setPreviewMode(id);
    }
  };

  const getTag = formValues.keys ? formValues.keys.split(',') : '';
  const getFilter = (array: IFilterType[], value: string): IFilterType => {
    let filter: IFilterType = { id: '', value: '' };
    array.forEach((element) => {
      if (element.value === value) {
        filter = { ...element };
      }
    });
    return filter;
  };

  return (
    <div>
      <Formik
        initialValues={formValues}
        validationSchema={ValidationSchema}
        onSubmit={(values, action): void => {
          const result: IPersonPOST200 = {
            id: values.id,
            name: values.name,
            description: values.description,
            imageURL: values.img,
            nameColor: values.nameColor,
            backgroundColor: values.backgroundColor,
            parametersColor: values.parametersColor,
            tag1: getTag[0],
            tag2: getTag[1],
            tag3: getTag[2],
            gender: getFilter(filtersDictionary.gender, values.gender),
            race: getFilter(filtersDictionary.race, values.race),
            side: getFilter(filtersDictionary.side, values.side),
          };

          setTimeout(() => {
            void PersonsProvider.addPerson(JSON.stringify(result, null, 2));
            action.setSubmitting(false);
          }, 100);

          dispatch(addPerson(values));
          onCloseHandler();
        }}
      >
        {({ values, errors, touched, handleChange, handleBlur, handleSubmit }) => {
          return (
            <Form className={classes.wrapper} onSubmit={handleSubmit} action="#">
              <div className={classes.form}>
                <label className={inputStyles.label} htmlFor={HtmlForId.NAME}>
                  {Labels.NAME}
                  <Field
                    className={inputStyles.input}
                    name={FieldName.NAME}
                    type={InputTypes.TEXT}
                    id={HtmlForId.NAME}
                    value={values[FieldName.NAME]}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors[FieldName.NAME] && touched[FieldName.NAME] ? (
                    <span className={classes.error}>{errors[FieldName.NAME]}</span>
                  ) : null}
                </label>
                <span className={classes.line}>
                  {Object.keys(filtersDictionary).map((filter, index) => {
                    return (
                      <label className={inputStyles.label} key={index} htmlFor={filter}>
                        {SelectHeaderTypeRUS[filter.toUpperCase()]}
                        <Field
                          className={inputStyles.input}
                          as="select"
                          name={filter}
                          id={filter}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values[filter]}
                        >
                          <option className={inputStyles.input} value="">
                            Выбрать...
                          </option>
                          {filtersDictionary[filter].map((element: IFilterType, index) => {
                            return (
                              <option
                                className={inputStyles.input}
                                key={index}
                                id={element.id}
                                label={element.value}
                                value={element.value}
                              >
                                {element.value}
                              </option>
                            );
                          })}
                        </Field>
                      </label>
                    );
                  })}
                </span>
                <label className={inputStyles.label} htmlFor={HtmlForId.DESCRIPTION}>
                  {Labels.DESCRIPTION}
                  <Field
                    className={inputStyles.input}
                    name={FieldName.DESCRIPTION}
                    type={InputTypes.TEXT}
                    id={HtmlForId.DESCRIPTION}
                    value={values[FieldName.DESCRIPTION]}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors[FieldName.DESCRIPTION] && touched[FieldName.DESCRIPTION] ? (
                    <span className={classes.error}>{errors[FieldName.DESCRIPTION]}</span>
                  ) : null}
                </label>
                <label className={inputStyles.label} htmlFor={HtmlForId.KEYS}>
                  {Labels.KEYS}
                  <Field
                    className={inputStyles.input}
                    name={FieldName.KEYS}
                    type={InputTypes.TEXT}
                    id={HtmlForId.KEYS}
                    value={values[FieldName.KEYS]}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  {errors[FieldName.KEYS] && touched[FieldName.KEYS] ? (
                    <span className={classes.error}>{errors[FieldName.KEYS]}</span>
                  ) : null}
                </label>
                <div className={classes.line}>
                  <div className={classes.image}>
                    {values.img !== '' ? <img src={values[FieldName.IMAGE]} alt={FieldName.IMAGE} /> : null}
                    <label className={inputStyles.label} htmlFor={HtmlForId.IMAGE}>
                      {Labels.IMAGE}
                      <Field
                        className={inputStyles.uploadText}
                        name={FieldName.IMAGE}
                        type={InputTypes.TEXT}
                        id={HtmlForId.IMAGE}
                        value={values[FieldName.IMAGE]}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      {errors[FieldName.IMAGE] && touched[FieldName.IMAGE] ? (
                        <span className={classes.error}>{errors[FieldName.IMAGE]}</span>
                      ) : null}
                    </label>
                  </div>
                  <div className={classes.color}>
                    <h3 className={classes.header}>Выбрать цвет</h3>
                    <label htmlFor={HtmlForId.BACKGROUND_COLOR}>
                      <span>{Labels.BACKGROUND_COLOR}</span>
                      <Field
                        className={inputStyles.input}
                        name={FieldName.BACKGROUND_COLOR}
                        type={InputTypes.COLOR}
                        id={HtmlForId.BACKGROUND_COLOR}
                        value={values[FieldName.BACKGROUND_COLOR]}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </label>
                    <label htmlFor={HtmlForId.NAME_COLOR}>
                      <span>{Labels.NAME_COLOR}</span>
                      <Field
                        className={inputStyles.input}
                        name={FieldName.NAME_COLOR}
                        type={InputTypes.COLOR}
                        id={HtmlForId.NAME_COLOR}
                        value={values[FieldName.NAME_COLOR]}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </label>
                    <label htmlFor={HtmlForId.PARAMETERS_COLOR}>
                      <span>{Labels.PARAMETERS_COLOR}</span>
                      <Field
                        className={inputStyles.input}
                        name={FieldName.PARAMETERS_COLOR}
                        type={InputTypes.COLOR}
                        id={HtmlForId.PARAMETERS_COLOR}
                        value={values[FieldName.PARAMETERS_COLOR]}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                    </label>
                  </div>
                </div>
              </div>
              <div className={classes.preview}>
                <h3 className={classes.header}>Предварительный просмотр</h3>
                <span className={classes.line}>
                  <h4 className={classes.header} id={PreviewMode.PRIMARY} onClick={onChangePreviewMode}>
                    Вид 1
                  </h4>
                  <h4 className={classes.header} id={PreviewMode.SECONDARY} onClick={onChangePreviewMode}>
                    Вид 2
                  </h4>
                </span>
                <div>
                  {previewMode === PreviewMode.PRIMARY ? (
                    <Card person={values} clickable={false} />
                  ) : (
                    <ModalCardContent
                      name={values.name}
                      gender={values.gender}
                      race={values.race}
                      side={values.side}
                      description={values.description}
                      imageUrl={values.img}
                      backgroundColor={values.backgroundColor}
                      keys={values.keys}
                      parametersColor={values.parametersColor}
                      nameColor={values.nameColor}
                    />
                  )}
                </div>
                <span className={classes.buttons}>
                  <Button
                    type={ButtonTypes.BUTTON}
                    styles={ButtonStylesTypes.beige}
                    text={'Отменить'}
                    handler={onCloseHandler}
                  />
                  <button
                    type={ButtonTypes.SUBMIT}
                    className={[buttonStyles.button, buttonStyles.button__green].join(' ')}
                  >
                    Отправить
                  </button>
                </span>
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export { ModalForm };
