export interface IModalCardContentProps {
  name: string;
  gender: string;
  race: string;
  side: string;
  description: string | undefined;
  imageUrl: string;
  backgroundColor: string;
  parametersColor: string;
  nameColor: string;
  keys: string | undefined;
}
