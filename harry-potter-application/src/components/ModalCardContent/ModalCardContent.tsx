import React from 'react';

import { CardContent, KeyTag } from '../index';

import { IModalCardContentProps } from './IModalCardContentProps';
import classes from './ModalCardContent.module.scss';

const ModalCardContent: React.FC<IModalCardContentProps> = ({
  name,
  gender,
  race,
  side,
  imageUrl,
  backgroundColor,
  parametersColor,
  nameColor,
  description,
  keys,
}): JSX.Element => {
  return (
    <div className={classes.wrapper}>
      <h3 className={classes.header} style={{ color: nameColor }}>
        {name}
      </h3>
      <div className={classes.cardModal}>
        <div className={classes.content}>
          <CardContent
            gender={gender}
            race={race}
            side={side}
            backgroundColor={backgroundColor}
            parametersColor={parametersColor}
          />
          {description ? <p className={classes.text}>{description}</p> : null}
        </div>
        <div className={classes.image}>
          <img src={imageUrl} alt={name} />
          <div className={classes.keys}>
            {keys
              ? keys.split(',').map((key): JSX.Element => {
                  return <KeyTag key={key} keyTag={key} />;
                })
              : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export { ModalCardContent };
