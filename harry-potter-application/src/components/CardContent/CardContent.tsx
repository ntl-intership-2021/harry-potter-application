import React from 'react';

import { CardContentField } from '../../shared/CardContentField';

import classes from './CardContent.module.scss';
import { ICardContentProps } from './ICardContentProps';

const CardContent: React.FC<ICardContentProps> = ({
  gender,
  race,
  side,
  backgroundColor,
  parametersColor,
}): JSX.Element => {
  return (
    <div className={classes.content} style={{ backgroundColor: backgroundColor, color: parametersColor }}>
      <p className={classes.paragraph}>
        <span>{CardContentField.GENDER}</span>
        <span>{gender}</span>
      </p>
      <p className={classes.paragraph}>
        <span>{CardContentField.RACE}</span>
        <span>{race}</span>
      </p>
      <p className={classes.paragraph}>
        <span>{CardContentField.SIDE}</span>
        <span>{side}</span>
      </p>
    </div>
  );
};

export { CardContent };
