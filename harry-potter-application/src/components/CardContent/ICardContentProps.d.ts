export interface ICardContentProps {
  gender: string;
  race: string;
  side: string;
  backgroundColor: string;
  parametersColor: string;
}
