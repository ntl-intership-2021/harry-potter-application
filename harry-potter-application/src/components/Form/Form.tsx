import React, { useEffect, useState } from 'react';

import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { IFilterSettings } from '../../interfaces/common/IFilterSettings';
import { SelectHeaderTypeRUS } from '../../interfaces/common/SelectHeaderTypeRUS';
import { DictionaryProvider } from '../../services/dictionary/DictionaryProvider';
import { PersonsProvider } from '../../services/persons/PersonsProvider';
import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';
import { InputTypes } from '../../shared/InputTypes';
import { addFilterSettings } from '../../store/slices/filterSlice';
import { addFilteredPersonsList } from '../../store/slices/personSlice';
import { Button, Input, Modal, ModalForm, Select } from '../index';

import classes from './Form.module.scss';

const HTML_FOR_ID = 'person-search';
const PLACEHOLDER = 'Поиск';

const Form: React.FC = (): JSX.Element => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [isInit, setIsInit] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState<string>('');
  const [filtersDictionary, setFiltersDictionary] = useState<IFilterSettings | undefined>();
  const [genderResponse, setGenderResponse] = useState<string | null>(null);
  const [raceResponse, setRaceResponse] = useState<string | null>(null);
  const [sideResponse, setSideResponse] = useState<string | null>(null);

  useEffect(() => {
    const getResponse = async (): Promise<void> => {
      try {
        const response = await PersonsProvider.getPerson(inputValue);
        dispatch(addFilteredPersonsList(response));
      } catch (e) {
        if (e instanceof Error) {
          throw Error(e.message);
        }
      }
    };
    if (isInit) {
      void getResponse();
    }
  }, [inputValue]);
  useEffect(() => {
    const getFilterDictionary = async (): Promise<void> => {
      try {
        const incomingFiltersDictionary: IFilterSettings = await DictionaryProvider.getAllDictionary();
        dispatch(addFilterSettings(incomingFiltersDictionary));
        setFiltersDictionary(incomingFiltersDictionary);
      } catch (e) {
        if (e instanceof Error) {
          throw Error(e.message);
        }
      }
    };
    void getFilterDictionary();
  }, []);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const target = event.target;
    const value = target.value;
    setInputValue(value);
    setIsInit(true);
  };
  const handleModal = (): void => {
    if (!modalOpen) {
      setModalOpen(true);
      history.push('persons/new');
    }
  };
  const handleModalClose = (): void => {
    if (modalOpen) {
      setModalOpen(false);
      history.push('/persons');
    }
  };
  const checkboxesHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const target = event.target;
    const id = target.id.split('__');
    const type = id[0];
    const hash = id[1];

    const checked = target.checked;
    if (checked && type === 'gender') {
      setGenderResponse(`gender=${hash}`);
    }
    if (checked && type === 'race') {
      setRaceResponse(`race=${hash}`);
    }
    if (checked && type === 'side') {
      setSideResponse(`side=${hash}`);
    }
  };
  const resetFormFilters = (): void => {
    setGenderResponse(null);
    setRaceResponse(null);
    setSideResponse(null);
  };
  const submitHandler = async (event: React.FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    const response: string[] = [];
    if (genderResponse) {
      response.push(genderResponse);
    }
    if (raceResponse) {
      response.push(raceResponse);
    }
    if (sideResponse) {
      response.push(sideResponse);
    }
    const filteredPersons = await PersonsProvider.getFilteredPersons(response.join('&'));
    dispatch(addFilteredPersonsList(filteredPersons));
    resetFormFilters();
  };

  return (
    <div className={classes.wrapper}>
      <form className={classes.form} onSubmit={submitHandler}>
        <Input
          onChange={handleChange}
          title={PLACEHOLDER}
          type={InputTypes.TEXT}
          htmlForID={HTML_FOR_ID}
          buttonContains={true}
          labelContains={false}
        />
        <div className={classes.line}>
          <div className={classes.select}>
            {filtersDictionary
              ? Object.keys(filtersDictionary).map((filter, index) => {
                  return (
                    <Select
                      key={index}
                      type={SelectHeaderTypeRUS[filter.toUpperCase()]}
                      engType={filter}
                      options={filtersDictionary[filter]}
                      checkboxesHandler={checkboxesHandler}
                    />
                  );
                })
              : null}
          </div>
        </div>
      </form>
      <Button styles={ButtonStylesTypes.beige} text={'Добавить'} type={ButtonTypes.BUTTON} handler={handleModal} />
      {modalOpen ? (
        <Modal modalCloseHandler={handleModalClose} innerElement={<ModalForm onCloseHandler={handleModalClose} />} />
      ) : null}
    </div>
  );
};

export { Form };
