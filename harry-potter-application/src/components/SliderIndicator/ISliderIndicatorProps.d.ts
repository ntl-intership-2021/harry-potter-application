import React from 'react';

export interface ISliderIndicatorProps {
  id: string;
  active: boolean;
  sliderCardsHandler: (event: React.MouseEvent) => void;
}
