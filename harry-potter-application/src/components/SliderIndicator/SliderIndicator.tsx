import React from 'react';

import { ButtonStylesTypes } from '../../shared/ButtonStylesTypes';
import { ButtonTypes } from '../../shared/ButtonTypes';
import { Button } from '../index';

import { ISliderIndicatorProps } from './ISliderIndicatorProps';
import classes from './SliderIndicator.module.scss';

const SliderIndicator: React.FC<ISliderIndicatorProps> = ({ id, active, sliderCardsHandler }) => {
  return (
    <div className={classes.indicator}>
      <Button
        styles={ButtonStylesTypes.white}
        text={'·'}
        type={ButtonTypes.BUTTON}
        id={id}
        style={active ? { color: '#b09a81' } : undefined}
        handler={sliderCardsHandler}
      />
    </div>
  );
};

export { SliderIndicator };
