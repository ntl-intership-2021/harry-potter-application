import React from 'react';

import { INavigationLink } from '../../interfaces/application/INavigationLink';
import { NAVIGATION_LINKS } from '../../shared/NavigationLinks';
import { NavigationLink } from '../index';

import classes from './Navigation.module.scss';

const Navigation: React.FC = (): JSX.Element => {
  return (
    <nav>
      <ul className={classes.navigation}>
        {NAVIGATION_LINKS.map((link: INavigationLink): JSX.Element => {
          return <NavigationLink key={link.eng} rus={link.rus} eng={link.eng} />;
        })}
      </ul>
    </nav>
  );
};

export { Navigation };
