import React, { useState } from 'react';

import { useHistory, useParams } from 'react-router-dom';

import { CardContent, Modal, ModalCardContent } from '../index';

import classes from './Card.module.scss';
import { ICardProps } from './ICardProps';

const Card: React.FC<ICardProps> = ({ person, clickable }) => {
  const [cardModalOpen, setCardModalOpen] = useState<boolean>(false);
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const cardModalHandler = (): void => {
    if (cardModalOpen) {
      setCardModalOpen(false);
      history.push('/persons');
    } else {
      setCardModalOpen(true);
      history.push(`/persons/${person.id}`);
    }
  };
  return (
    <div className={classes.card} style={{ backgroundColor: person.backgroundColor }}>
      <div className={classes.wrapper} onClick={cardModalHandler}>
        <img className={classes.image} src={person.img} alt={person.name} />
        <h3 className={classes.header} style={{ color: person.nameColor }}>
          {person.name}
        </h3>
        <CardContent
          gender={person.gender}
          race={person.race}
          side={person.side}
          backgroundColor={person.backgroundColor}
          parametersColor={person.parametersColor}
        />
      </div>
      {cardModalOpen && clickable ? (
        <Modal
          modalCloseHandler={cardModalHandler}
          innerElement={
            <ModalCardContent
              name={person.name}
              gender={person.gender}
              race={person.race}
              side={person.side}
              description={person.description}
              imageUrl={person.img}
              backgroundColor={person.backgroundColor}
              parametersColor={person.parametersColor}
              nameColor={person.name}
              keys={person.keys}
            />
          }
        />
      ) : null}
    </div>
  );
};

export { Card };
