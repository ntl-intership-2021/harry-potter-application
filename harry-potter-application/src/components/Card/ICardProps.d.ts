import { IPerson } from '../../interfaces/common/IPersons';

export interface ICardProps {
  person: IPerson;
  clickable: boolean;
}
