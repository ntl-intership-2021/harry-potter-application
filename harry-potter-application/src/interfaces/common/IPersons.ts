export interface IPerson {
  id: string;
  name: string;
  gender: string;
  race: string;
  side: string;
  backgroundColor: string;
  nameColor: string;
  parametersColor: string;
  img: string;
  description?: string;
  keys?: string;
}
