export interface IFilterType {
  id: string;
  value: string;
}
