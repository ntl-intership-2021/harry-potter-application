import { IFilterType } from './IFilterType';

export interface IFilterSettings {
  gender: IFilterType[];
  race: IFilterType[];
  side: IFilterType[];
}
