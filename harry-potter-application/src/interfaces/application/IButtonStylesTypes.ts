export interface IButtonStylesTypes {
  beige: string;
  green: string;
  icon: string;
  white: string;
  active: string;
}
