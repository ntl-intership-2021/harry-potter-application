export interface IPersonGET200 {
  backgroundColor: string;
  gender: string;
  id: string;
  imageURL: string;
  name: string;
  nameColor: string;
  parametersColor: string;
  race: string;
  side: string;
}
