import { IPageableGET200 } from './IPageableGET200';
import { IPersonGET200 } from './IPersonGET200';
import { ISortGET200 } from './ISortGET200';

export interface IDataGET200 {
  content: IPersonGET200[];
  empty: boolean;
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  pageable: IPageableGET200;
  size: number;
  sort: ISortGET200;
  totalElements: number;
  totalPages: number;
}
