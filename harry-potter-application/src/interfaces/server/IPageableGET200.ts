import { ISortGET200 } from './ISortGET200';

export interface IPageableGET200 {
  offset: number;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  sort: ISortGET200;
  unpaged: boolean;
}
