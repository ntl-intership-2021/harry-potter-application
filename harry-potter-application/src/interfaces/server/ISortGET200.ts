export interface ISortGET200 {
  unsorted: boolean;
  sorted: boolean;
  empty: boolean;
}
