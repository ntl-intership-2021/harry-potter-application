export interface IPersonPOST200 {
  id: string;
  name: string;
  description?: string;
  imageURL: string;
  nameColor: string;
  backgroundColor: string;
  parametersColor: string;
  tag1: string;
  tag2: string;
  tag3: string;
  gender: {
    id: string;
    value: string;
  };
  race: {
    id: string;
    value: string;
  };
  side: {
    id: string;
    value: string;
  };
}
