import { IFilterSettings } from '../common/IFilterSettings';

export interface IFiltersStoreState {
  filtersDictionary: IFilterSettings;
}
