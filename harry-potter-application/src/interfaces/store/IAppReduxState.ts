import { IFiltersStoreState } from './IFiltersStoreState';

import { IPersonsStoreState } from './IPersonsStoreState';

export interface IAppReduxState {
  personsList: IPersonsStoreState;
  filters: IFiltersStoreState;
}
