import { IPerson } from '../common/IPersons';

export interface IPersonsStoreState {
  persons: IPerson[] | [];
}
